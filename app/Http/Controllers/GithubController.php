<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Http;
use Storage;

class GithubController extends Controller
{
    /**
     * Handle webhook from Github.
     *
     * @param  int  $id  Gitlab project id
     */
    public function webhook(Request $request, int $id)
    {
        chdir(storage_path('app'));  // 工作目錄切換到 storage
        // 先從 Github clone 下來
        shell_exec("git clone " . $request['repository']['clone_url'] . " 2>&1");
        $cloned_path = storage_path('app/' . $request['repository']['name']);  // Path of clone repo from Github
        chdir($cloned_path);
        // hook 來的太快就像來不及更新狀態的店家，進去才知道沒了
        sleep(3);
        shell_exec('git pull');

        // 設定 remote 成 Gitlab
        $gitlab_remote = str_replace('github', 'gitlab', $request['repository']['clone_url']);
        shell_exec('git remote set-url origin ' . $gitlab_remote . ' 2>&1');

        $reflection_json = null;

        // PR 事件
        if (!is_null($request['pull_request']) && !is_null($request['pull_request']['merged_at'])) {
            $new_pr = $this->pr($request, $id);
            $reflection_json = $new_pr;
            \Log::info($new_pr);
        }

        // 完事後記得清理乾淨
        Storage::deleteDirectory($cloned_path);

        return response()->json($reflection_json);
    }

    /**
     * Handle webhook from Github.
     *
     * @param  int  $id  Gitlab project id
     */
    public function pr(Request $request, int $id)
    {
        // 建立分支
        $source_branch = "pr-" . $request['pull_request']['number'];
        shell_exec('git checkout -b ' . $source_branch . ' 2>&1');  // TODO: 進化成 switch
        shell_exec('git push -u https://reflection-bot:' . env('GITLAB_TOKEN') . '@gitlab.com/' . $request['repository']['full_name'] . '.git ' . $source_branch . ' 2>&1');

        // Create MR
        $response = Http::withToken(env('GITLAB_TOKEN'))
            ->post('https://gitlab.com/api/v4/projects/' . $id . '/merge_requests', [
                'id' => $id,
                'source_branch' => $source_branch,
                'target_branch' => $request['repository']['default_branch'],
                'title' => "[Reflection] Github PR " . $request['pull_request']['number'] . '：' . $request['pull_request']['title'],
                'description' =>  $request['pull_request']['body'],
                'label' => env('GITLAB_LABEL', 'reflection'),
            ]);

        if ($response->failed()) {
            if ($response->clientError())
                return response()->json($response, 400);
            elseif ($response->serverError())
                return response()->json($response, 500);
        }

        return $response;
    }
}
